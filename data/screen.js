var lines = [];
var aimes = [];
var starts = [];
function dstCalc(start_lat, start_lng, end_lat, end_lng){
	//distance between two coordinates
	const R = 6371e3; // metres
        const φ1 = start_lat * Math.PI/180; // φ, λ in radians
        const φ2 = end_lat * Math.PI/180;
        const Δφ = (end_lat-start_lat) * Math.PI/180;
        const Δλ = (end_lng-start_lng) * Math.PI/180;

        const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                Math.cos(φ1) * Math.cos(φ2) *
                Math.sin(Δλ/2) * Math.sin(Δλ/2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return Math.floor(R * c); // in metres
}
const startIcon = L.icon({
			iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Twemoji12_1f4cd.svg/240px-Twemoji12_1f4cd.svg.png',
 			iconSize: [20, 20],
 			iconAnchor: [10, 20],
			popupAnchor: [0, -10],
			text: "needle"
		});
const littleStation = L.icon({
			iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Eo_circle_blue_white_circle.svg/240px-Eo_circle_blue_white_circle.svg.png',
			iconSize: [30, 30],
			iconAnchor: [20, 20],
			popupAnchor: [0, -20]
		});
function drawLine(start_lat,start_lng,end_lat,end_lng, texts){
    console.log(texts);
let beginTime = new Date(texts[0]*1000);
let endTime = new Date(texts[1]*1000);
let startStr=beginTime.getDate()+"/"+(beginTime.getMonth()+1)+"/"+beginTime.getFullYear()+" "+beginTime.getHours()+":"+beginTime.getMinutes()+":"+beginTime.getSeconds()
let endStr=endTime.getDate()+"/"+(endTime.getMonth()+1)+"/"+endTime.getFullYear()+" "+endTime.getHours()+":"+endTime.getMinutes()+":"+endTime.getSeconds()
var line = L.polyline([[start_lat,start_lng],[end_lat,end_lng]]).addTo(map);
line.bindPopup("NR "+texts[2]+"<br>"+startStr+" to "+endStr+"<br>Distance:"+dstCalc(start_lat,start_lng,end_lat,end_lng)+" m");
var startMarker = L.marker([start_lat, start_lng], {icon:startIcon}).addTo(map);
startMarker.bindPopup("START<br>"+startStr);
var aimMarker = L.marker([end_lat, end_lng], {icon:startIcon}).addTo(map);
aimMarker.bindPopup("END<br>"+endStr);
aimes.push(aimMarker);
starts.push(startMarker);
lines.push(line);
}

function dialog(head, text){
		  document.getElementById('diaHead').innerHTML = head;
		  document.getElementById('diaContent').innerHTML = text;
		  blockEl("dialog-overlay");
}

async function getMap(cityID){
//give out a list of all current rentals of the user
return new Promise((resolve, reject) => {
    //rent a bike
    var reacted = false;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://api.nextbike.net/maps/nextbike-live.json?city="+cityID);
    //xhr.setRequestHeader("Accept", "application/json");
    //xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
    if (xhr.status == 200) {
		resolve(JSON.parse(xhr.responseText));
	}
	
	}
	xhr.send()
});
}
