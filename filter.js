function checkAllTypes(over){
    //check all type checkboxes which are inside of the field, if the box is uncheck, also uncheck all sub boxes.
    const id = over.id.replace('group', '42');
    const check = over.checked
    var parent = document.getElementById(id).children
    for (let i = 0; i < parent.length; i++) {
        const box = parent[i].firstChild
        box.checked = check
        filter.add(box.id.replace("type", ""))
    }
}
class Filter{
    //reulate all kinds of the view, which is defined by get parameters
    constructor(){
        this.active = false;
        this.types = this.readTypes()
        this.night = nightactive()
        this.transit = transitactive()
        this.cluster = true;
        this.empty = this.checkEmpty()
        if(!(this.checkCluster()))this.cluster = false;
        //prevents outfilter everything, when unused.
        if(this.types.length != 0)this.active = true;
    }
    toggleNight(){
        if(this.night){
            this.night=false;
        }else{
            this.night = true
        }

    }
    toggleTransit(){
        if(this.transit){
            this.transit=false;
        }else{
            this.transit = true
        }


    }
    toggleEmpty(){
        if(this.empty){
            this.empty=false;
        }else{
            this.empty = true
        }
    }
    toggleCluster(){
        if(this.cluster){
            this.cluster=false;
        }else{
            this.cluster = true
        }
    }
    readTypes(){
        const url = new URL(window.location.href);
        if(url.searchParams.get("types") == null)return [];

        var lst = url.searchParams.get("types").split(',');
        lst.pop()
        return lst
    }
    checkCluster(){
        return !(location.href.includes("cluster"))
    }
    checkEmpty(){
        return !(location.href.includes("empty"))
    }
    typesCSV(){
        var csv = ''
        this.types.forEach(function(typus){
            csv+=typus+','
        })
        return csv
    }
    setURL(commit){
        var params = window.location.hash.split("#");
        const nextUrl = (this.night ? "?night&" : "?")+(!(this.cluster)?"cluster&":"")+(!(this.empty)?"empty&":"")+(this.transit ? "transit&":"")+'types='+this.typesCSV()+'#'+params[1]+(typeof params[2] !== "undefined" ? "#"+params[2] : "")
        if(commit){
            //if commit is set, reload the page.
            location.href = nextUrl
            return;
        }
        try{
            //... else, only change the adress line
            window.history.replaceState(nextUrl, "Filter", "/new-url");
        }catch{
            //if history api not implemented, use old api
            console.log("no history api.")
            this.setURL(true)
        }
    }
    setCheck(type){
        return inPatch(String(type),this.types)
    }
    add(type){
        //add type into the filter list and update url
        console.log(this.types)
        if(this.setCheck(type)){
            this.remove(type);
            return false;
        }
        this.types.push(String(type));
        this.active = true;
        return true

    }
    remove(type){
        console.log("remove")
        //remove type into the filter list and update url
        if(!(this.setCheck(type))){
            console.log("add");
            this.add(type);
            return false;
        }
        delete this.types[this.types.indexOf(String(type))]
        if(this.types.length == 0)this.active = false;
        return true;
    }
}
